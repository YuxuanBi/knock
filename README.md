# 技术点梳理：从萌新到混子
这个项目经历了4个月。对于一个新人来说，这个项目从构建到部署，从技术框架到语言细节都是全新的。项目已经结束了，我试图归纳梳理项目上遇到的技术点，因此开了这个坑，以萌新的视角介绍我在这个项目上遇到和解决过的问题。
1. 用GitLab实现CI-CD
2. 用helm部署pod到kubernetes上
3. SpringBoot+Kotlin语法槽点
4. Kafka流处理结合Springboot和Kotlin的实现
5. 数据库部署和应用从一到多
6. 利用TestContainer或EmbedKafka或DockerCompose写令人脑阔炸裂的集成测试
7. E2ETest，Vault，数据库批处理，Kibana，NewRelic，Optimizely等杂项

源码：
https://gitlab.com/YuxuanBi/zip-project-rethink
